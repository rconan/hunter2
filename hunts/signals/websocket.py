# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
from itertools import chain

from django.core.exceptions import ObjectDoesNotExist
from django.db import connections, transaction
from django.db.models import Q
from django.db.models.signals import pre_save, post_save, pre_delete, m2m_changed, post_delete
from django.dispatch import receiver
from django_tenants.utils import get_tenant_database_alias

from hunts import models
from hunts.websocket.consumers import HuntWebsocket, PuzzleEventWebsocket
from teams.models import Team


def pre_save_handler(func):
    """The purpose of this decorator is to connect signals to the type of handlers we use here.

    Before the normal signature of the signal handler, func is passed "old", the instance in the
    database before save was called (or None). func will then be called after the current
    transaction has been successfully committed, ensuring that the instance argument is stored in the
    database and accessible via database connections in other threads, and that data is ready to be
    sent to clients."""
    def inner(sender, instance, *args, **kwargs):
        try:
            old = type(instance).objects.get(pk=instance.pk)
        except ObjectDoesNotExist:
            old = None

        def after_commit():
            func(old, sender, instance, *args, **kwargs)

        if transaction.get_autocommit():
            # in this case we want to wait until *post* save so the new object is in the db, which on_commit
            # will not do. Instead, do nothing but set an attribute on the instance to the callback, and
            # call it later in a post_save receiver.
            instance._hybrid_save_cb = after_commit
        else:  # nocover
            transaction.on_commit(after_commit)

    return inner


@receiver(post_save)
def hybrid_save_signal_dispatcher(sender, instance, **kwargs):
    # This checks for the attribute set by the above signal handler and calls it if it exists.
    hybrid_cb = getattr(instance, '_hybrid_save_cb', None)
    if hybrid_cb:
        # No need to pass args because this will always be a closure with the args from pre_save
        instance._hybrid_save_cb = None
        hybrid_cb()


@pre_save_handler
def saved_announcement(old, sender, announcement, raw, *args, **kwargs):
    if raw:  # nocover
        return
    # Announcement doesn't have a foreign key to the tenant, but the ID is required to build the channel layer keys
    event = connections[get_tenant_database_alias()].tenant
    HuntWebsocket.send_announcement_msg(event, announcement.puzzle, announcement)


def deleted_announcement(sender, instance, *args, **kwargs):
    # Announcement doesn't have a foreign key to the tenant, but the ID is required to build the channel layer keys
    event = connections[get_tenant_database_alias()].tenant
    HuntWebsocket.send_delete_announcement_msg(event, instance.puzzle, instance)


pre_save.connect(saved_announcement, sender=models.Announcement)
pre_delete.connect(deleted_announcement, sender=models.Announcement)


@pre_save_handler
def saved_teampuzzleprogress(old, sender, progress, raw, *args, **kwargs):
    if raw:  # nocover
        return

    if progress.solved_by and (not old or not old.solved_by):
        PuzzleEventWebsocket.send_solved(progress)


# handler: Guess.pre_save
@pre_save_handler
def saved_guess(old, sender, guess, raw, *args, **kwargs):
    # Do not trigger unless this was a newly created guess.
    # Note this means an admin modifying a guess will not trigger anything.
    if raw:  # nocover
        return
    if old:
        return

    PuzzleEventWebsocket.send_new_guess(guess)


@pre_save_handler
def saved_teamunlock(old, sender, teamunlock, raw, *args, **kwargs):
    if raw:  # nocover:
        return

    if not teamunlock.unlockanswer.unlock.hidden:
        PuzzleEventWebsocket.send_new_unlock(teamunlock)

    dependent = teamunlock.unlockanswer.unlock.hint_set.all()
    obsoleted = teamunlock.unlockanswer.unlock.obsoletes.filter(
        Q(start_after__in=teamunlock.team_puzzle_progress.teamunlock_set.values('unlockanswer__unlock')) |
        Q(start_after=None)
    )
    hints = chain(dependent, obsoleted)
    PuzzleEventWebsocket.schedule_hints(
        teamunlock.team_puzzle_progress.puzzle,
        teamunlock.team_puzzle_progress.team_id,
        hints=hints,
        send_expired=True
    )


def deleted_teamunlock(sender, instance, *args, **kwargs):
    teamunlock = instance
    # TODO: this incurs at least one query each time this handler runs, which runs many times in some situations
    # like if the unlock itself is deleted, and/or teams had several guesses unlocking it multiple times

    unlock = teamunlock.unlockanswer.unlock
    progress = teamunlock.team_puzzle_progress
    # First handle dependent hints
    for hint in unlock.hint_set.seal().all():
        PuzzleEventWebsocket.cancel_scheduled_hint(progress.puzzle, progress.team_id, hint=hint)
    # ... then obsoleted hints. They require more logic, and queries.
    for hint in unlock.obsoletes.select_related('puzzle', 'puzzle__episode').prefetch_related('obsoleted_by').seal().all():
        if not hint.obsolete_for(progress.team, progress):
            # If the unlock obsoletes the hint and the hint is no longer obsolete for the team, the team needs to
            # be notified. But there are two possible target states: a normal hint and no hint.
            if hint.unlocked_by(progress.team, progress):
                # i.e. the hint should go back to normal
                PuzzleEventWebsocket.send_new_hint(progress.team_id, hint, hint in progress.accepted_hints.all(), False)
            elif hint.start_after_id is None or hint.start_after_id == teamunlock.unlockanswer.unlock_id:
                # i.e. the hint is no longer unlocked. Note that if hint.start_after is something else,
                # the hint *was not unlocked, and not visible, before this deletion.*
                PuzzleEventWebsocket.send_delete_hint(progress.team_id, hint)
    # To avoid doing more than necessary, we don't explicitly check if the unlock is still unlocked.
    # The client deletes unlocks from its store if there are no guesses unlocking them and
    # determines what to display based on the contents of the unlock order which we are asserting
    # the state of anyway.
    # This could result in some unlocks remaining visible incorrectly if there is a network interruption,
    # where doing these checks would mean we can always send a "delete unlock" event to the client.
    # Since this is rare and we assume the team has usually seen the unlock and gained any benefit
    # or confusion (if it's being changed because it was wrong!) from it, this is probably OK.
    PuzzleEventWebsocket.send_delete_unlockguess(teamunlock)


# handler: Unlock.pre_save
@pre_save_handler
def saved_unlock(old, sender, unlock, raw, *args, **kwargs):
    if raw:  # nocover
        return
    if not old:
        return

    if unlock.puzzle != old.puzzle:
        raise ValueError("Cannot move unlocks between puzzles")
    # Get list of teams which can see this unlock
    team_unlocks = models.TeamUnlock.objects.filter(
        unlockanswer__unlock=unlock
    ).select_related(
        'team_puzzle_progress',
        'unlockanswer__unlock__puzzle__episode',
        'unlocked_by',
    ).prefetch_related(
        'team_puzzle_progress__teamunlock_set__unlockanswer',
    ).seal()

    for team_unlock in team_unlocks:
        PuzzleEventWebsocket.send_change_unlock(team_unlock)


# handler: Hint.pre_save
@pre_save_handler
def saved_hint(old, sender, instance, raw, *args, **kwargs):
    if raw:  # nocover
        return
    hint = instance
    if old and hint.puzzle != old.puzzle:
        raise NotImplementedError

    tpps = models.TeamPuzzleProgress.objects.filter(
        puzzle=hint.puzzle
    ).select_related(
        'team', 'puzzle'
    ).prefetch_related(
        'accepted_hints',
        'teamunlock_set__unlockanswer'
    ).seal()
    for progress in tpps:
        if hint.unlocked_by(progress.team, progress):
            PuzzleEventWebsocket.cancel_scheduled_hint(hint.puzzle, progress.team_id, hint=hint)
            accepted = hint in progress.accepted_hints.all()
            obsolete = hint.obsolete_for(progress.team, progress)
            # Rather than try to work out whether the client should change what it's displaying and only update
            # it if so, just send the info and let it decide.
            PuzzleEventWebsocket.send_new_hint(progress.team_id, hint, accepted, obsolete)
        else:
            if old and old.unlocked_by(progress.team, progress):
                PuzzleEventWebsocket.send_delete_hint(progress.team_id, hint)
            PuzzleEventWebsocket.schedule_hints(
                hint.puzzle, progress.team_id, hints=[hint], send_expired=True
            )


# handler: Hint.pre_delete
def deleted_hint(sender, instance, *arg, **kwargs):
    hint = instance

    for team in Team.objects.all():
        try:
            progress = hint.puzzle.teampuzzleprogress_set.get(team=team)
        except models.TeamPuzzleProgress.DoesNotExist:
            continue

        if hint.unlocked_by(team, progress):
            PuzzleEventWebsocket.send_delete_hint(team.id, hint)


# handler: TeamPuzzleProgress.accepted_hints.through.m2m_changed
def accepted_hint(sender, instance, action, pk_set, **kwargs):
    # hints will never be "unaccepted" in normal flow, so don't handle this scenario
    if action == 'post_add':
        team = instance.team
        for hint in models.Hint.objects.filter(pk__in=pk_set):
            obsolete = hint.obsolete_for(team, instance)
            PuzzleEventWebsocket.send_new_hint(team.id, hint, True, obsolete)


pre_save.connect(saved_teampuzzleprogress, sender=models.TeamPuzzleProgress)
pre_save.connect(saved_teamunlock, sender=models.TeamUnlock)
pre_save.connect(saved_guess, sender=models.Guess)
pre_save.connect(saved_unlock, sender=models.Unlock)
pre_save.connect(saved_hint, sender=models.Hint)

post_delete.connect(deleted_teamunlock, sender=models.TeamUnlock)
pre_delete.connect(deleted_hint, sender=models.Hint)

m2m_changed.connect(accepted_hint, sender=models.TeamPuzzleProgress.accepted_hints.through)
