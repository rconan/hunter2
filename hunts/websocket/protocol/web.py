# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.duration import duration_iso_string


def new_announcement(announcement):
    return {
        'type': 'announcement',
        'content': {
            'announcement_id': announcement.id,
            'title': announcement.title,
            'message': announcement.message,
            'notify': announcement.notify.name,
            'variant': announcement.type.variant,
        },
    }


def delete_announcement(announcement):
    return {
        'type': 'delete_announcement',
        'content': {
            'announcement_id': announcement.id
        },
    }


def new_unlock(teamunlock):
    return {
        'type': 'new_unlock',
        'content': _unlock(teamunlock),
    }


def old_unlock(teamunlock):
    return {
        'type': 'old_unlock',
        'content': _unlock(teamunlock),
    }


def change_unlock(teamunlock):
    return {
        'type': 'change_unlock',
        'content': _unlock(teamunlock),
    }


def _unlock(teamunlock):
    return {
        'guess': teamunlock.unlocked_by.guess,
        'unlock': teamunlock.unlockanswer.unlock.text,
        'unlock_uid': teamunlock.unlockanswer.unlock.compact_id,
        'unlock_order': teamunlock.team_puzzle_progress.unlock_order(),
    }


def delete_unlockguess(teamunlock):
    return {
        'type': 'delete_unlockguess',
        'content': {
            'guess': teamunlock.unlocked_by.guess,
            'unlock_uid': teamunlock.unlockanswer.unlock.compact_id,
            'unlock_order': teamunlock.team_puzzle_progress.unlock_order(),
        },
    }


def _guess(guess):
    return {
        'timestamp': str(guess.given),
        'guess': guess.guess,
        'guess_uid': guess.compact_id,
        'by': guess.by.username,
    }


def new_guesses(guesses):
    return {
        'type': 'new_guesses',
        'content': [_guess(guess) for guess in guesses],
    }


def old_guesses(guesses):
    return {
        'type': 'old_guesses',
        'content': [_guess(guess) for guess in guesses],
    }


def solved(progress):
    content = {
        'time': (progress.solved_by.given - progress.start_time).total_seconds(),
        'guess': progress.solved_by.guess,
        'by': progress.solved_by.by.username
    }
    episode = progress.puzzle.episode
    next = episode.next_puzzle(progress.team)
    if next:
        next = episode.get_puzzle(next)
        content['text'] = 'to the next puzzle'
        content['redirect'] = next.get_absolute_url()
    else:
        content['text'] = f'back to {episode.name}'
        content['redirect'] = episode.get_absolute_url()

    return {
        'type': 'solved',
        'content': content,
    }


def _hint(hint, accepted, obsolete):
    return {
        'hint': hint.text if accepted else None,
        'hint_uid': hint.compact_id,
        'accepted': accepted,
        'obsolete': obsolete,
        'time': duration_iso_string(hint.time),
        'depends_on_unlock_uid': hint.start_after.compact_id if hint.start_after else None
    }


def new_hint(hint, accepted, obsolete):
    return {
        'type': 'new_hint',
        'content': _hint(hint, accepted, obsolete)
    }


def old_hint(hint, accepted, obsolete):
    return {
        'type': 'old_hint',
        'content': _hint(hint, accepted, obsolete)
    }


def delete_hint(hint):
    return {
        'type': 'delete_hint',
        'content': {
            'hint_uid': hint.compact_id,
            'depends_on_unlock_uid': hint.start_after.compact_id if hint.start_after else None
        },
    }


def cooldown(end_time, last_guessed):
    return {
        'type': 'cooldown',
        'content': {
            'end_time': (end_time.timestamp() * 1000) if end_time else None,
            'last_guessed': (last_guessed.timestamp() * 1000) if last_guessed else None,
        }
    }
