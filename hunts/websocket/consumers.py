# Copyright (C) 2018-2021 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from datetime import datetime
from datetime import timezone as dt_timezone

import inflection
from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from channels.layers import get_channel_layer
from django.db.models import Prefetch, Case, When, Value
from django.utils import timezone

from events.consumers import EventMixin
from hunts import models, utils
from teams.consumers import TeamMixin
from .protocol import web as web_protocol


class BaseWebsocket(JsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _error(self, message):
        self.send_json({'type': 'error', 'content': {'error': message}})

    @classmethod
    def _send_message(cls, group, message):
        layer = get_channel_layer()
        async_to_sync(layer.group_send)(group, {'type': 'send_json_msg', 'content': message})

    def send_json_msg(self, content, close=False):
        # For some reason consumer dispatch doesn't strip off the outer dictionary with 'type': 'send_json'
        # (or whatever method name) so we override and do it here. This saves us having to define a separate
        # method which just calls send_json for each type of message.
        super().send_json(content['content'])

    def receive_json(self, content, **kwargs):
        if 'type' not in content:
            self._error('no type in message')
            return

        if content['type'][-4:] != '-plz':
            self._error('impolite request')
            return

        try:
            # Extract the message type before -plz and convert to a handler function name starting handle_
            # The static prefix prevents selection of in-built attributes which could lead to security vulnerabilities
            type = inflection.underscore(content['type'][:-4])
            handler = getattr(self, f"handle_{type}")
        except AttributeError:
            self._error('invalid request')
            return

        handler(content, **kwargs)


class HuntWebsocket(EventMixin, TeamMixin, BaseWebsocket):
    def connect(self):
        async_to_sync(self.channel_layer.group_add)(
           self._announcement_groupname(self.scope['tenant']), self.channel_name
        )
        super().connect()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self._announcement_groupname(self.scope['tenant']), self.channel_name
        )

    @classmethod
    def _announcement_groupname(cls, event, puzzle=None):
        if puzzle:
            return f'event-{event.id}.puzzle-{puzzle.id}.announcements'
        else:
            return f'event-{event.id}.announcements'

    @classmethod
    def _send_message(cls, group, message):
        layer = get_channel_layer()
        async_to_sync(layer.group_send)(group, {'type': 'send_json_msg', 'content': message})

    def send_json_msg(self, content, close=False):
        # For some reason consumer dispatch doesn't strip off the outer dictionary with 'type': 'send_json'
        # (or whatever method name) so we override and do it here. This saves us having to define a separate
        # method which just calls send_json for each type of message.
        super().send_json(content['content'])

    @classmethod
    def send_announcement_msg(cls, event, puzzle, announcement):
        cls._send_message(cls._announcement_groupname(event, puzzle), web_protocol.new_announcement(announcement))

    @classmethod
    def send_delete_announcement_msg(cls, event, puzzle, announcement):
        cls._send_message(cls._announcement_groupname(event, puzzle), web_protocol.delete_announcement(announcement))


class PuzzleEventWebsocket(HuntWebsocket):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hint_events = {}

    async def __call__(self, scope, receive, send):
        scope['loop'] = asyncio.get_running_loop()
        await super().__call__(scope, receive, send)

    @classmethod
    def puzzle_groupname(cls, puzzle, team_id=None):
        event_id = puzzle.episode.event_id
        if team_id:
            return f'event-{event_id}.puzzle-{puzzle.id}.events.team-{team_id}'
        else:
            return f'event-{event_id}.puzzle-{puzzle.id}.events'

    def connect(self):
        keywords = self.scope['url_route']['kwargs']
        episode_number = keywords['episode_number']
        puzzle_number = keywords['puzzle_number']
        self.episode, self.puzzle = utils.event_episode_puzzle(self.scope['tenant'], episode_number, puzzle_number)
        async_to_sync(self.channel_layer.group_add)(
            self.puzzle_groupname(self.puzzle, self.team.id), self.channel_name
        )
        async_to_sync(self.channel_layer.group_add)(
            self._announcement_groupname(self.episode.event, self.puzzle), self.channel_name
        )
        self.setup_hint_timers()
        super().connect()

    def disconnect(self, close_code):
        super().disconnect(close_code)
        for e in self.hint_events.values():
            e.cancel()
        async_to_sync(self.channel_layer.group_discard)(
            self.puzzle_groupname(self.puzzle, self.team.id), self.channel_name
        )

    def handle_guesses(self, content, **kwargs):
        if 'from' not in content:
            self._error('required field "from" is missing')
            return
        self.send_old_guesses(content['from'])

    def handle_unlocks(self, content, **kwargs):
        self.send_old_unlocks()

    def handle_hints(self, content, **kwargs):
        if 'from' not in content:
            self._error('required field "from" is missing')
            return
        self.send_old_hints(content['from'])

    def handle_cooldown(self, content, **kwargs):
        self.send_cooldown()

    def _get_progress_for_hints(self):
        now = timezone.now()
        progress, _ = models.TeamPuzzleProgress.objects.prefetch_related(
            'teamunlock_set__unlockanswer',
            'accepted_hints',
        ).seal().get_or_create(
            puzzle=self.puzzle,
            team=self.team,
            defaults={
                'start_time': now,
                'late': now > self.episode.event.end_date,
            }
        )
        return progress

    def setup_hint_timers(self):
        hints = self.puzzle.hint_set.all().select_related('start_after').prefetch_related('obsoleted_by').seal()
        progress = self._get_progress_for_hints()
        for hint in hints:
            self.schedule_hint(hint, progress)

    @classmethod
    def schedule_hints(cls, puzzle, team_id, hints, send_expired):
        layer = get_channel_layer()
        groupname = cls.puzzle_groupname(puzzle, team_id)
        async_to_sync(layer.group_send)(
            groupname, {
                'type': 'schedule_hints_real',
                'hint_uids': [str(hint.id) for hint in hints],
                'send_expired': send_expired,
            }
        )

    def schedule_hints_real(self, message):
        # select_related is needed not for performance but so no queries are necessary in the
        # event loop, which is not allowed
        hints = models.Hint.objects.select_related('start_after').filter(id__in=message['hint_uids'])
        send_expired = message.get('send_expired', False)
        progress = self._get_progress_for_hints()
        for hint in hints:
            self.schedule_hint(hint, progress, send_expired)

    def schedule_hint(self, hint, progress, send_expired=False):
        try:
            self.hint_events[hint.id].cancel()
            del self.hint_events[hint.id]
        except KeyError:
            pass

        delay = hint.delay_for_team(self.team, progress)
        if delay is None:
            return
        accepted = hint in progress.accepted_hints.all()
        obsolete = hint.obsolete_for(self.team, progress)
        delay = delay.total_seconds()
        if not send_expired and delay < 0:
            return

        # Prepare JSON outside the event loop (because it's not possible inside)
        # for any hidden unlocks which this hint depends on in order to send them along with the hint.
        tu_dicts = [
            web_protocol.new_unlock(tu)
            for tu in progress.teamunlock_set.filter(
                unlockanswer__unlock_id=hint.start_after_id,
                unlockanswer__unlock__text=''
            )
        ]
        # run the hint sender function on the asyncio event loop so we don't have to bother writing scheduler stuff
        # we need a future in order to be able to cancel it. This seems to be the easiest way to get one.
        future = asyncio.run_coroutine_threadsafe(
            self.send_new_hint_after_delay(self.team, hint, tu_dicts, accepted, obsolete, delay),
            self.scope['loop'],
        )
        self.hint_events[hint.id] = future

    @classmethod
    def cancel_scheduled_hint(cls, puzzle, team_id, hint):
        layer = get_channel_layer()
        groupname = cls.puzzle_groupname(puzzle, team_id)
        async_to_sync(layer.group_send)(groupname, {
            'type': 'cancel_scheduled_hint_real',
            'hint_uid': str(hint.id),
        })

    def cancel_scheduled_hint_real(self, message):
        hint = models.Hint.objects.get(id=message['hint_uid'])

        try:
            self.hint_events[hint.id].cancel()
            del self.hint_events[hint.id]
        except KeyError:
            pass

    @classmethod
    def send_new_unlock(cls, teamunlock):
        cls._send_message(
            cls.puzzle_groupname(teamunlock.team_puzzle_progress.puzzle, teamunlock.team_puzzle_progress.team_id),
            web_protocol.new_unlock(teamunlock)
        )

    @classmethod
    def send_delete_unlockguess(cls, teamunlock):
        cls._send_message(
            cls.puzzle_groupname(teamunlock.unlockanswer.unlock.puzzle, teamunlock.unlocked_by.by_team_id),
            web_protocol.delete_unlockguess(teamunlock)
        )

    @classmethod
    def send_new_guess(cls, guess):
        cls._send_message(cls.puzzle_groupname(guess.for_puzzle, guess.by_team_id), web_protocol.new_guesses([guess]))

    @classmethod
    def send_solved(cls, progress):
        cls._send_message(cls.puzzle_groupname(progress.puzzle, progress.team_id), web_protocol.solved(progress))

    @classmethod
    def send_change_unlock(cls, teamunlock):
        cls._send_message(
            cls.puzzle_groupname(teamunlock.unlockanswer.unlock.puzzle, teamunlock.team_puzzle_progress.team_id),
            web_protocol.change_unlock(teamunlock),
        )

    @classmethod
    def send_new_hint(cls, team_id, hint, accepted, obsolete):
        cls._send_message(cls.puzzle_groupname(hint.puzzle, team_id), web_protocol.new_hint(hint, accepted, obsolete))

    @classmethod
    def send_delete_hint(cls, team_id, hint):
        cls._send_message(cls.puzzle_groupname(hint.puzzle, team_id), web_protocol.delete_hint(hint))

    async def send_new_hint_after_delay(self, team, hint, tu_dicts, accepted, obsolete, delay, **kwargs):
        # We can't have a sync function (added to the event loop via call_later) because it would have to call back
        # ultimately to SyncConsumer's send method, which is wrapped in async_to_sync, which refuses to run in a thread
        # with a running asyncio event loop.
        # See https://github.com/django/asgiref/issues/56
        await asyncio.sleep(delay)

        # If the hint depends on a hidden unlock, we won't have sent the unlock guess when it was unlocked.
        if hint.start_after and hint.start_after.hidden:
            for tu in tu_dicts:
                await self.base_send.awaitable(
                    {'type': 'websocket.send', 'text': self.encode_json(tu)}
                )
        # AsyncConsumer replaces its own base_send attribute with an async_to_sync wrapped version if the instance is (a
        # subclass of) SyncConsumer. While bizarre, the original async function is available as AsyncToSync.awaitable.
        # We also have to reproduce the functionality of JsonWebsocketConsumer and WebsocketConsumer here (they don't
        # have async versions.)
        await self.base_send.awaitable(
            {'type': 'websocket.send', 'text': self.encode_json(web_protocol.new_hint(hint, accepted, obsolete))}
        )
        del self.hint_events[hint.id]

    def send_old_guesses(self, start):
        guesses = models.Guess.objects.filter(for_puzzle=self.puzzle, by_team=self.team).order_by('given')
        if start != 'all':
            start = datetime.fromtimestamp(int(start) / 1000, dt_timezone.utc)
            # TODO: `start` is given by the client and is the timestamp of the most recently received guess.
            # the following could miss guesses if guesses get the same timestamp, though this is very unlikely.
            guesses = guesses.filter(given__gt=start).select_related('by', 'correct_for').seal()

            # If the puzzle was solved since the time requested, inform the client.
            try:
                progress = models.TeamPuzzleProgress.objects.select_related('puzzle', 'solved_by__by').seal().get(
                    puzzle=self.puzzle, team=self.team)
                # .get() doesn't transfer members to the retrieved object: avoid select_related for them
                progress.puzzle = self.puzzle
                progress.team = self.team
                if progress.solved_by and progress.solved_by.given > start:
                    self.send_json(web_protocol.solved(progress))
            except models.TeamPuzzleProgress.DoesNotExist:
                pass

            # The client requested guesses from a certain point in time, i.e. it already has some.
            # Even though these are "old" they're "new" in the sense that the user will never have
            # seen them before so should trigger the same UI effect.
            self.send_json(web_protocol.new_guesses(guesses))
        else:
            guesses = guesses.select_related('by').seal()
            self.send_json(web_protocol.old_guesses(guesses))

    def send_old_hints(self, start):
        progress = self.puzzle.teampuzzleprogress_set.select_related(
            'team'
        ).prefetch_related(
            'accepted_hints',
            'puzzle__hint_set__obsoleted_by',
            Prefetch(
                'teamunlock_set',
                queryset=models.TeamUnlock.objects.select_related(
                    'unlocked_by',
                    'unlockanswer',
                    'unlockanswer__unlock',
                ).seal()
            ),
        ).seal().get(team=self.team)
        hints = [h for hint_list in progress.hints().values() for h in hint_list]
        if start != 'all':
            start = datetime.fromtimestamp(int(start) // 1000, dt_timezone.utc)
            # The following finds the hints which were *not* unlocked at the start time given.
            # combined with the existing filter this gives the ones the client might have missed.
            hints = [h for h in hints if progress.start_time + h.time > start]
            protocol_func = web_protocol.new_hint
        else:
            protocol_func = web_protocol.old_hint

        for h in hints:
            self.send_json(protocol_func(h, h.accepted, h.obsolete))

    def send_old_unlocks(self):
        progress = models.TeamPuzzleProgress.objects.get(team=self.team, puzzle=self.puzzle)

        teamunlocks = models.TeamUnlock.objects.filter(
            team_puzzle_progress=progress,
        ).annotate(
            hidden=Case(
                When(unlockanswer__unlock__text='', then=Value(True)),
                default_value=Value(False),
            )
        ).order_by(
            'unlockanswer__unlock__text',
        ).select_related(
            'team_puzzle_progress',
            'unlockanswer__unlock',
            'unlocked_by',
        ).prefetch_related(
            'team_puzzle_progress__teamunlock_set__unlockanswer',
        ).seal()
        hints = progress.hints()

        for tu in teamunlocks:
            if not tu.hidden or hints[tu.unlockanswer.unlock_id]:
                self.send_json(web_protocol.old_unlock(tu))

    def send_cooldown(self):
        behaviour = self.puzzle.episode.get_cooldown_behaviour()
        end_time = behaviour.end_time(self.scope['user'], self.team, self.puzzle)
        last_guessed = behaviour.last_guessed(self.scope['user'], self.team, self.puzzle)
        self.send_json(web_protocol.cooldown(end_time, last_guessed))
