# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from datetime import timedelta
from urllib.parse import unquote, urlparse

from channels.testing import WebsocketCommunicator, ApplicationCommunicator
from django.core.management import call_command
from django.db import connections
from django.test import TransactionTestCase
from django.utils import timezone
from django_tenants.test.cases import FastTenantTestCase
from django_tenants.test.client import TenantClient

from .factories import EventFactory, DomainFactory
from .models import Event


TEST_SCHEMA_NAME = '_unittest_schema'


class EventAwareTestCase(TransactionTestCase):
    @staticmethod
    def _delete_events_and_schemas():
        """Delete all events and drop their associated schemas

        Note "all": this allows tests to be sure there are no other events in the database messing things up,
        and also helps keep these tests fast (see note in truncate_tables), but imposes a cost when we switch back to
        the fast TestCase and makes --reuse-db less effective.
        """
        Event.deactivate()
        for event in Event.objects.all():
            event.delete(force_drop=True)

    def _fixture_setup(self):
        # We have to delete events on setup because the default fast TestCase leaves its Event lying around to be
        # reused, which tends to break tests that expect to have a clean event table.
        self._delete_events_and_schemas()
        super()._fixture_setup()

    def _fixture_teardown(self):
        self._delete_events_and_schemas()
        # As well as dropping the event schema(s) and the event from the public schema, the public schema may have
        # been populated with other stuff so still needs to be cleaned.
        self.truncate_tables()

    @classmethod
    def truncate_tables(cls):
        db_names = cls._databases_names(include_mirrors=False)
        for db_name in db_names:
            conn = connections[db_name]
            inhibit_post_migrate = (
                cls.available_apps is not None or
                (
                    # Inhibit the post_migrate signal when using serialized
                    # rollback to avoid trying to recreate the serialized data.
                    cls.serialized_rollback and
                    hasattr(conn, '_test_serialized_contents')
                )
            )
            conn.set_schema_to_public()
            # The default truncation behaviour in the superclass does not (generally) cascade. This is wrong in our
            # case because there are always FKs from the event schemas to the public schemas, so it's not possible
            # to truncate the public schema without cascading.
            # NOTE: TRUNCATE ... CASCADE is quite slow if there are a lot of tables. It is therefore important that
            # not only are tables truncated, but that *schemas are dropped*
            call_command(
                'flush',
                verbosity=0,
                interactive=False,
                database=db_name,
                reset_sequences=False,
                allow_cascade=True,
                inhibit_post_migrate=inhibit_post_migrate
            )


class EventTestCase(FastTenantTestCase):
    def _pre_setup(self):
        super()._pre_setup()

        self.client = TenantClient(self.tenant)

    @classmethod
    def setup_tenant(cls, tenant):
        tenant.current = True
        tenant.end_date = timezone.now() + timedelta(days=5)
        tenant.name = 'Test Event'

    @classmethod
    def get_test_schema_name(cls):
        return TEST_SCHEMA_NAME


class ScopeOverrideCommunicator(WebsocketCommunicator):
    def __init__(self, application, path, scope=None, headers=None, subprotocols=None):
        if not isinstance(path, str):
            raise TypeError("Expected str, got {}".format(type(path)))
        if scope is None:
            scope = {}
        parsed = urlparse(path)
        self.scope = {
            "type": "websocket",
            "path": unquote(parsed.path),
            "query_string": parsed.query.encode("utf-8"),
            "headers": headers or [],
            "subprotocols": subprotocols or [],
        }
        self.scope.update(scope)
        ApplicationCommunicator.__init__(self, application, self.scope)


class AsyncEventTestCase(EventAwareTestCase):
    """TestCase to support consumer tests

    This cannot use transactions to achieve test isolation, because the signal handlers need to call across threads
    to use the asyncio-based consumer, hence transactions must be committed for db changes to show up consistently.

    However, we don't want to create an Event every test, because setting up and tearing down the schema and its
    tables is very slow, so this class is something of a hybrid between the EventAware- and Event-TestCases.
    """
    def setUp(self):
        self.client = TenantClient(self.tenant)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        Event.deactivate()
        try:
            cls.loop = asyncio.get_event_loop()
        except RuntimeError:
            raise RuntimeError('Could not create asyncio event loop; '
                               'something is messing with event loop policy which probably means'
                               'tests will not run as expected.')
        cls.tenant = EventFactory(max_team_size=2)
        cls.domain = DomainFactory(tenant=cls.tenant)
        cls.tenant.activate()
        cls.headers = [
            (b'origin', b'hunter2.local'),
            (b'host', cls.domain.domain.encode('idna'))
        ]

    def _fixture_setup(self):
        pass

    def _fixture_teardown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        cls._delete_events_and_schemas()
        cls.truncate_tables()
        super().tearDownClass()

    def get_communicator(self, app, url, scope=None):
        return ScopeOverrideCommunicator(app, url, scope, headers=self.headers)

    def receive_json(self, comm, msg='', no_fail=False):
        try:
            output = self.run_async(comm.receive_json_from)()
        except asyncio.TimeoutError:
            if no_fail:
                return {}
            else:
                self.fail(msg)
        return output

    def run_async(self, coro):
        async def wrapper(result, *args, **kwargs):
            try:
                r = await coro(*args, **kwargs)
            except Exception as e:
                result.set_exception(e)
            else:
                result.set_result(r)

        def inner(*args, **kwargs):
            result = asyncio.Future()
            if not self.loop.is_running():
                try:
                    self.loop.run_until_complete(wrapper(result, *args, **kwargs))
                finally:
                    pass
            else:
                raise RuntimeError('Event loop was already running. '
                                   'AsyncEventTestCase always stops the loop, '
                                   'so something else is using it in a way this is not designed for.')
            return result.result()

        return inner
