# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import pytest
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.providers.steam.provider import SteamOpenIDProvider
from django.apps import apps
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from faker import Faker
from pytest_django.asserts import assertInHTML

from accounts.factories import UserFactory
from accounts.models import UserProfile, User
from events.factories import AttendanceFactory
from events.models import Attendance
from events.test import EventTestCase
from hunter2.models import Configuration
from teams.factories import TeamMemberFactory
from teams.models import TeamRole


class FactoryTests(TestCase):
    def test_user_factory_default_construction(self):
        UserFactory.create()

    def test_user_factory_no_real_name(self):
        UserFactory.create(first_name="", last_name="")


class ProfilePageTests(EventTestCase):
    def test_participant(self):
        user = TeamMemberFactory(team__role=TeamRole.PLAYER)
        team = user.team_at(self.tenant)
        AttendanceFactory(user=user, event=self.tenant)
        response = self.client.get(reverse('profile', kwargs={'uuid': user.uuid}))
        self.assertEqual(200, response.status_code)
        self.assertInHTML(
            f'<h2>Participant in</h2><ul><li>{team.name} @ {self.tenant.name}</li></ul>',
            response.content.decode('utf-8')
        )

    def test_admin(self):
        user = TeamMemberFactory(team__role=TeamRole.ADMIN)
        AttendanceFactory(user=user, event=self.tenant)
        response = self.client.get(reverse('profile', kwargs={'uuid': user.uuid}))
        self.assertEqual(200, response.status_code)
        self.assertInHTML(
            f'<h2>Admin at</h2><ul><li>{self.tenant.name}</li></ul>',
            response.content.decode('utf-8')
        )

    def test_author(self):
        user = TeamMemberFactory(team__role=TeamRole.AUTHOR)
        AttendanceFactory(user=user, event=self.tenant)
        response = self.client.get(reverse('profile', kwargs={'uuid': user.uuid}))
        self.assertEqual(200, response.status_code)
        self.assertInHTML(
            f'<h2>Author of</h2><ul><li>{self.tenant.name}</li></ul>',
            response.content.decode('utf-8')
        )


class AdminRegistrationTests(TestCase):
    def test_models_registered(self):
        models = apps.get_app_config('accounts').get_models()
        # Models which don't need to be registered due to being deprecated and retained only for old data migration
        exclude_models = (UserProfile,)
        for model in models:
            if model not in exclude_models:
                self.assertIsInstance(admin.site._registry[model], admin.ModelAdmin)


@pytest.mark.usefixtures('db')
class TestSignup:

    @pytest.fixture
    def password(self, faker):
        return faker.password()

    def test_signup_saves_contact_choice_and_privacy_policy_version(self, client, faker, password):
        response = client.post(
            reverse('account_signup'),
            {
                'username': faker.user_name(),
                'email': faker.email(),
                'password1': password,
                'password2': password,
                'contact': 'False',  # False is more likely to be translated to None by accident than True
                'privacy': 'on',
            },
        )
        assert response.status_code == 302  # Should redirect back to where you were after signup
        user = get_user_model().objects.get()
        assert user.contact is not None
        assert user.accepted_privacy_policy_version == Configuration.get_solo().privacy_policy_version

    def test_signup_without_privacy(self, client, faker, password):
        config = Configuration.get_solo()
        config.privacy_policy = faker.paragraph()
        config.save()
        response = client.post(
            reverse('account_signup'),
            {
                'username': faker.user_name(),
                'email': faker.email(),
                'password1': password,
                'password2': password,
                'contact': faker.boolean(),
                'privacy': 'off',
            },
        )
        assert response.status_code == 400

    def test_signup_with_missing_captcha(self, client, faker, password):
        config = Configuration.get_solo()
        config.captcha_question = faker.paragraph()
        config.captcha_answer = faker.paragraph()
        config.save()
        response = client.post(
            reverse('account_signup'),
            {
                'username': faker.user_name(),
                'email': faker.email(),
                'password1': password,
                'password2': password,
                'contact': faker.boolean(),
            },
        )
        assert response.status_code == 200  # 200, strangely, indicates the form was rejected
        assertInHTML('<li>This field is required.</li>', response.content.decode('utf-8'))

    def test_signup_with_wrong_captcha(self, client, faker, password):
        config = Configuration.get_solo()
        config.captcha_question = faker.paragraph()
        config.captcha_answer = faker.paragraph()
        config.save()
        response = client.post(
            reverse('account_signup'),
            {
                'username': faker.user_name(),
                'email': faker.email(),
                'password1': password,
                'password2': password,
                'contact': faker.boolean(),
                'captcha': faker.paragraph(),
            },
        )
        assert response.status_code == 200  # 200, strangely, indicates the form was rejected
        assertInHTML('<li>You must correctly answer this question</li>', response.content.decode('utf-8'))

    def test_signup_with_right_captcha(self, client, faker, password):
        config = Configuration.get_solo()
        config.captcha_question = faker.paragraph()
        config.captcha_answer = faker.paragraph().lower()
        config.save()
        response = client.post(
            reverse('account_signup'),
            {
                'username': faker.user_name(),
                'email': faker.email(),
                'password1': password,
                'password2': password,
                'contact': faker.boolean(),
                'captcha': config.captcha_answer.title(),
            },
        )
        assert response.status_code == 302


@pytest.mark.usefixtures('db')
class TestUserManagement:
    def test_create_superuser(self, faker):
        configuration = Configuration.get_solo()
        configuration.privacy_policy_version = faker.random_int()
        configuration.save()
        u = User.objects.create_superuser(faker.user_name(), faker.email(), faker.password())
        assert u.accepted_privacy_policy_version == configuration.privacy_policy_version


@pytest.mark.usefixtures('db')
class TestAutocomplete:
    @pytest.fixture
    def user_a(self):
        return UserFactory(username='a_user', email='a_email@example.org')

    @pytest.fixture
    def user_b(self):
        return UserFactory(username='b_user', email='a_email@example.com')

    @pytest.fixture
    def querier(self):
        return UserFactory(username='a_querier', email='a_querier@example.com')

    @pytest.fixture
    def staff(self):
        return UserFactory(is_staff=True, username='a_staff', email='a_staff@example.com')

    def test_autocomplete_uuid_from_username(self, client, user_a, user_b, querier):
        client.force_login(querier)
        url = reverse('user_autocomplete')
        response = client.get(f'{url}?q=a')
        assert response.status_code == 200
        results = response.json()['results']
        assert len(results) == 1
        assert results[0]['id'] == str(user_a.uuid)

    def test_autocomplete_id_non_staff(self, client, querier):
        client.force_login(querier)
        url = reverse('user_id_autocomplete')
        response = client.get(f'{url}?q=a')
        assert response.status_code == 403

    def test_autocomplete_id_from_username(self, client, user_a, user_b, staff):
        client.force_login(staff)
        url = reverse('user_id_autocomplete')
        response = client.get(f'{url}?q=a')
        assert response.status_code == 200
        results = response.json()['results']
        assert len(results) == 2
        assert set(r['id'] for r in results) == {str(user_a.id), str(staff.id)}


class EditProfileTests(EventTestCase):
    def setUp(self):
        self.fake = Faker()

    def test_edit_profile_update_fields(self):
        user = TeamMemberFactory()
        attendance = AttendanceFactory(user=user, event=self.tenant)
        self.client.force_login(user)

        url = reverse('edit_profile')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        new_email = self.fake.email()
        new_contact = self.fake.boolean()
        new_picture = self.fake.image_url()
        new_seat = self.fake.postcode()  # It looks kinda like a seat number?

        self.client.post(url, {
            'email': new_email,
            'contact': new_contact,
            'picture': new_picture,
            'attendance_set-TOTAL_FORMS': 1,
            'attendance_set-INITIAL_FORMS': 1,
            'attendance_set-MIN_NUM_FORMS': 0,
            'attendance_set-MAX_NUM_FORMS': 1000,
            'attendance_set-0-id': attendance.id,
            'attendance_set-0-user': user.id,
            'attendance_set-0-seat': new_seat,
        })
        self.assertEqual(response.status_code, 200)

        new_user = get_user_model().objects.get(id=user.id)
        new_attendance = Attendance.objects.get(id=attendance.id)
        self.assertEqual(new_user.email, new_email)
        self.assertEqual(new_user.contact, new_contact)
        self.assertEqual(new_user.picture, new_picture)
        self.assertEqual(new_attendance.seat, new_seat)


class TestAccountMiddleware:
    @pytest.fixture
    def steam_api_key(self, settings, faker):
        settings.STEAM_API_KEY = faker.random_letters()

    @pytest.mark.usefixtures('steam_api_key')
    def test_matching_version_update_steam_data(self, tenant_client, faker, monkeypatch):
        def mock_steam_data(_api_key, uid):
            username = faker.user_name()
            return {
                'steamid': uid,
                'personaname': username,
                'profileurl': f'https://steamcommunity.com/id/{username}',
            }

        monkeypatch.setattr('accounts.middleware.request_steam_account_summary', mock_steam_data)

        configuration = Configuration.get_solo()
        user = UserFactory(accepted_privacy_policy_version=configuration.privacy_policy_version)
        steam_account = SocialAccount(user=user, provider=SteamOpenIDProvider.id, uid=faker.random_number(digits=18, fix_len=True))
        steam_account.save()
        assert steam_account.extra_data == {}
        tenant_client.force_login(user)
        response = tenant_client.get(reverse('index'))
        assert response.status_code == 200
        steam_account.refresh_from_db()
        assert steam_account.extra_data != {}

    def test_matching_version_no_redirect(self, tenant_client):
        configuration = Configuration.get_solo()
        user = UserFactory(accepted_privacy_policy_version=configuration.privacy_policy_version)
        tenant_client.force_login(user)
        response = tenant_client.get(reverse('index'))
        assert response.status_code == 200

    def test_old_version_redirect(self, tenant_client):
        configuration = Configuration.get_solo()
        user = UserFactory(accepted_privacy_policy_version=configuration.privacy_policy_version - 1)
        tenant_client.force_login(user)
        url = reverse('index')
        response = tenant_client.get(url)
        assert response.status_code == 302
        redirect_url = reverse('review_privacy_policy')
        assert response.headers.get('Location') == f'{redirect_url}?next=%2F'

    def test_null_version_redirect(self, tenant_client):
        user = UserFactory(accepted_privacy_policy_version=None)
        tenant_client.force_login(user)
        url = reverse('index')
        response = tenant_client.get(url)
        assert response.status_code == 302
        redirect_url = reverse('review_privacy_policy')
        assert response.headers.get('Location') == f'{redirect_url}?next=%2F'

    def test_load_privacy_policy_review(self, tenant_client):
        configuration = Configuration.get_solo()
        user = UserFactory(accepted_privacy_policy_version=configuration.privacy_policy_version - 1)
        tenant_client.force_login(user)
        response = tenant_client.get(reverse('review_privacy_policy'))
        assert response.status_code == 200
        assert configuration.privacy_policy in response.content.decode('utf-8')

    def test_update_privacy_policy_accepted_version_redirect_index(self, tenant_client):
        configuration = Configuration.get_solo()
        user = UserFactory(accepted_privacy_policy_version=configuration.privacy_policy_version - 1)
        tenant_client.force_login(user)
        response = tenant_client.post(reverse('review_privacy_policy'), {'accept': 'on'})
        assert response.status_code == 302
        assert response.headers.get('Location') == reverse('index')
        user.refresh_from_db()
        assert user.accepted_privacy_policy_version == configuration.privacy_policy_version

    def test_update_privacy_policy_accepted_version_redirect_next(self, tenant_client):
        configuration = Configuration.get_solo()
        user = UserFactory(accepted_privacy_policy_version=configuration.privacy_policy_version - 1)
        tenant_client.force_login(user)
        path = reverse('review_privacy_policy')
        response = tenant_client.post(f'{path}?next=%2Fsomewhere', {'accept': 'on'})
        assert response.status_code == 302
        assert response.headers.get('Location') == '/somewhere'
        user.refresh_from_db()
        assert user.accepted_privacy_policy_version == configuration.privacy_policy_version
