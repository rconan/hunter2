# Copyright (C) 2022-2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import sentry_sdk
from allauth.socialaccount.providers.steam.provider import request_steam_account_summary, SteamOpenIDProvider
from django.conf import settings
from django.shortcuts import redirect
from django_tenants.urlresolvers import reverse

from hunter2.utils import add_query


class AccountMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        review_path = reverse('review_privacy_policy')
        url = add_query(review_path, {'next': request.path})
        if request.user.is_authenticated and request.path != review_path:
            if (
                request.user.accepted_privacy_policy_version is None or
                request.user.accepted_privacy_policy_version < request.site_configuration.privacy_policy_version
            ):
                return redirect(url)
            # We only want to request/store new data about existing accounts for users who have accepted the latest privacy policy
            # As a result, we also can't do so for users requesting the review page, so this is included here
            if settings.STEAM_API_KEY is not None:
                for account in request.user.socialaccount_set.filter(provider=SteamOpenIDProvider.id, extra_data={}):
                    account.extra_data = request_steam_account_summary(settings.STEAM_API_KEY, account.uid)
                    if not account.extra_data:
                        sentry_sdk.set_context("steam_request", {
                            "steam_id": account.uid,
                        })
                        sentry_sdk.capture_message("Steam API returned empty account information")
                    account.save()
        return self.get_response(request)
