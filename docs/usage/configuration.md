# Configuration

## Environment Variable Settings

hunter2 has a number of configuration options exposed via environment variables.
These are best configured in a file named `.env` alongside the `docker-compose.yml`.

The following options are available for a production setup:

| Environment Variable      | Required | Description                                                                                                                  | Default     |
|---------------------------|----------|------------------------------------------------------------------------------------------------------------------------------|-------------|
| `H2_DOMAIN`               | ✔️       | The root domain for the instance (without `www.` or `event.`)                                                                |             |
| `H2_DATABASE_PASSWORD`    | ✔️       | The password for the instance to connect to its database. The role must have full access to read/write the hunter2 database. |             |
| `H2_EMAIL_URL`            | ✔️       | A URL of an SMTP server for sending email, eg. smtp+tls://<username>:<password>@my.email.provider:587                        |             |
| `H2_DISCORD_APP`          | ❌        | Discord OAuth2 Client Information in the form: `<Client ID>,<Client Secret>`                                                 |             |
| `H2_STEAM_API_KEY`        | ❌        | Steam Web API Key for fetching profile information for Steam OpenID accounts.                                                |             |
| `H2_EMAIL_VERIFICATION `  | ❌        | One of 'none', 'optional' or 'mandatory' indicating whether users must verify their email address                            | 'mandatory' |
| `H2_DB_EXPORTER_PASSWORD` | ❌        | The password for database monitoring to connect to the database. The role is best setup using the installation instructions. |             |
| `H2_PIWIK_HOST`           | ❌        | The hostname of an instance of Matomo to collect analytics data                                                              |             |
| `H2_PIWIK_SITE`           | ❌        | The site number within the Matomo installation to report to                                                                  |             |
| `H2_SENTRY_DSN`           | ❌        | The URL of a Sentry DSN to report internal server errors and client JavaScript errors to                                     |             |
| `H2_SCHEME`               | ❌        | Override the scheme (protocol) in links to the site (eg. 'http' or 'https')                                                  | 'http'      |

### Steam Authentication
Hunter 2 supports two different modes for steam authentication. One using a generic OpenID authentication provider and another which is customised for steam, specifically.
Using the steam-specific provider required a Steam Web API Key configured via the `H2_STEAM_API_KEY`.
The two providers are not compatible with each other's data so if the API key is added/removed existing user data will need to be migrated.

When the API key is added generic OpenID accounts can be migrated to steam-specific ones using the following management command:
```
docker compose run --rm websocket migrate_steam_openid
```
If API key is removed again then accounts can be migrated back using:
```
docker compose run --rm websocket migrate_steam_openid --reverse
```

## Admin site settings

### Static Captcha
As a basic protection against bot signups the site can be configured with a static captcha question.
The best way to use this is to think of a question which all of your attendees should know the answer to but a generic bot would not.

```{figure} img/crud_captcha.png
:width: 1200
:alt: Django admin page showing static captcha configuration

Adding a static captcha
```
