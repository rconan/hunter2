#!/bin/sh

. ../venv/bin/activate
poetry install --no-root --no-interaction --verbose
DJANGO_SETTINGS_MODULE=hunter2.settings sphinx-build -v -b html docs docs/_build/html
